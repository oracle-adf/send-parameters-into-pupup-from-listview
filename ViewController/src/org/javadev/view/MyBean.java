package org.javadev.view;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupFetchEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;

public class MyBean {

    public MyBean() {
    }
    
    private String myVar;

    public void setMyVar(String myVar) {
        this.myVar = myVar;
    }

    public String getMyVar() {
        return myVar;
    }
    
    private RichOutputText myOutput;

    public void setMyOutput(RichOutputText myOutput) {
        this.myOutput = myOutput;
    }

    public RichOutputText getMyOutput() {
        return myOutput;
    }

    public void selectionEvent(SelectionEvent selectionEvent) {
        System.out.println("======================");
        System.out.println("SELECTION LISTENER");
        System.out.println("======================");

        System.out.println("========== myVAR ============");
        System.out.println(myVar);
        System.out.println("========== END ============");
        
    }

    public void onButtonClick(ActionEvent actionEvent) {
        setMyVar("Hello World!"); 
        System.out.println(myVar);
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(myOutput); // pass java binding of  ui component
    }
}
